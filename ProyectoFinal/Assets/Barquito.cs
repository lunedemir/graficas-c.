﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barquito : MonoBehaviour {

	float speed;
	// Use this for initialization
	void Start () {
		speed = .5f;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate ((Vector3.forward*Time.deltaTime)*speed);
	}
}
