﻿Shader "Custom/TexturedShader" {
	Properties {
		_Textured("Textured",2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _Textured;

		struct Input {
			float2 uv_Textured;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			o.Albedo = tex2D(_Textured, IN.uv_Textured).rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
