﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cambia : MonoBehaviour {

	public Material[] materiales;
	public Renderer r;
	public float speed;
	public Light luz;
	public float x;


	// Use this for initialization
	void Start () {
		//luz
		luz=GameObject.Find("Directional Light").GetComponent<Light>();
		luz.enabled = false;

		//materiales
		r = GetComponent<Renderer>();
		r.material = materiales [0];

		r.material.SetFloat ("_MySlider", 10f);

		//invoca el cambio de shaders
		Invoke ("Luces", 5.0F);
		Invoke ("CambiarA", 10.0f);
		Invoke ("CambiarB", 15.0f);
		Invoke ("CambiarC", 20.0f);
		Invoke ("CambiarD", 30.0f);

		Invoke ("Finito", 60f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (0,(Time.deltaTime*speed)+.2f,0,Space.World);
	}


	void Luces()
	{
		luz.enabled = true;
	}
	void CambiarA()//diffuse2
	{
		r.material = materiales [1];

		if (x >= 1) {
			r.material.SetFloat ("_MySlider", x-=.2f * Time.deltaTime);
		} else
			return;
	}
	void CambiarB()//texture
	{
		r.material = materiales [2];
	}
	void CambiarC()//normal map
	{
		r.material = materiales [3];
	}
	void CambiarD()//texture+normal map
	{
		r.material = materiales [4];
	}

	void Finito()
	{
		print ("FIN");
	}
}
