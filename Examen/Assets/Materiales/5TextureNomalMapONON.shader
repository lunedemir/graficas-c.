﻿Shader "Custom/5TextureNomalMapONON" {
	Properties {
		_NormalMap("Normal map", 2D)="bump"{}
		_NormalMapIntensity("Normal intensity",Range(0,1))=0
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed("x speed", Range(0,10))=.5
		_ScrollYSpeed("y speed", Range(0,10))=0
		_Color("Color", Color)=(1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo=tex2D(_MainTex,IN.uv_MainTex).rgb;
			float3 normalMap=UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			normalMap.x += _NormalMapIntensity;
			normalMap.y += _NormalMapIntensity;
			o.Normal=normalize(normalMap);
			fixed2 scrolledUV=IN.uv_MainTex;
			fixed xScrollValue=_ScrollXSpeed * _Time;
			fixed yScrollValue=_ScrollYSpeed*_Time;
			scrolledUV+=fixed2(xScrollValue, yScrollValue);
			half4 c = tex2D(_MainTex, scrolledUV);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
