﻿Shader "Custom/4NormalMapONOFF" {
	Properties {
		//_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Color ("Color", Color)=(200,200,200,1)
		_NormalMap("Normal map", 2D)="bump"{}
		_NormalMapIntensity("Normal intensity", Range(0,1))=0
		_ScrollXSpeed("X Scroll Speed", Range(0,10))=.5
		_ScrollYSpeed("Y Scroll Speed", Range(0,10))=.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		float _NormalMapIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		fixed4 _Color;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {


			float3 normalMap=UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			normalMap.x += _NormalMapIntensity;
			normalMap.y += _NormalMapIntensity;
			o.Normal=normalize(normalMap);

			fixed2 scrolledUV=IN.uv_NormalMap;
			fixed xScrollValue=_ScrollXSpeed * _Time;
			fixed yScrollValue=_ScrollYSpeed*_Time;
			scrolledUV+=fixed2(xScrollValue, yScrollValue);
			half4 c = tex2D(_NormalMap, scrolledUV);

			o.Albedo=_Color.rgb;


		}
		ENDCG
	}
	FallBack "Diffuse"
}
