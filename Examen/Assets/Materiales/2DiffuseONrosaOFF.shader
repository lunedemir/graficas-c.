﻿Shader "Custom/2DiffuseONrosaOFF" {
	Properties {
		_Color ("Color", Color) = (0,0,0,0)
		_AmbientColor ("Ambient Color", Color) = (0,0,0,0)
		_MySlider ("slider", Range (1,10))=2.5

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};


		fixed4 _Color;
		fixed4 _AmbientColor;
		float _MySlider;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c= pow((_Color+_AmbientColor),_MySlider); 
			o.Albedo = c.rgb;
			//o.Albedo= _ColorNegro.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
