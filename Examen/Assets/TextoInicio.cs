﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextoInicio : MonoBehaviour {

	public Text Shader;
	public Text Light;
	public Text Color;
	public Text Texture;
	public Text Tiempo;

	public float tiemp;
	public MeshRenderer r;
	public Light prende;
	public string onoff;
	public string texturita;
	public string col;

	// Use this for initialization
	void Start () {
		tiemp = 0.0f;
		r= GameObject.Find ("Sphere").GetComponent<MeshRenderer> ();

		//shader
		prende = GameObject.Find ("Directional Light").GetComponent<Light> ();		
	}
	
	// Update is called once per frame
	void Update () {
		//light
		if (prende.enabled==false)
			onoff = "OFF";
		else
			onoff = "ON";
		
		//tiempo
		tiemp += Time.deltaTime;

		//color
		col = r.material.color.ToString ();

		//textura
		if (r.material.GetTexture("_MainTex"))
			texturita = "ON";
		else
			texturita = "OFF";

		//texto
		Shader.text = "Shader: " + r.material.shader.name;
		Light.text = "Light: " + onoff;
		Color.text = "Color: " + col;
		Texture.text = "Texture: " + texturita;
		Tiempo.text = "Time: "+ tiemp.ToString();
	}
}
